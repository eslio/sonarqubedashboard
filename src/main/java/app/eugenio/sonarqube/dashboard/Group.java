package app.eugenio.sonarqube.dashboard;

public class Group {

    private String name;

    public Group(String name) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
