package app.eugenio.sonarqube.dashboard;

import org.junit.Test;
import static org.junit.Assert.*;

public class GroupTest {

    @Test
    public void testConstructor() {
        final String groupName = "potato";
        Group group = new Group(groupName);
        assertEquals(group.getName(), groupName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullConstructor() {
        Group group = new Group(null);
    }

}
